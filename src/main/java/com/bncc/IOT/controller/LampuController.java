package com.bncc.IOT.controller;

import com.bncc.IOT.domain.Lampu;
import com.bncc.IOT.service.LampuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/lampus")
public class LampuController {

    @Autowired
    private LampuService lampuService;

    @GetMapping
    public List<Lampu> findAll() {
        return lampuService.getAllLampu();
    }

    @PutMapping("/{id}")
    public Lampu update(@PathVariable int id, @RequestBody Lampu lampu){
        return lampuService.updateLampu(id, lampu);
    }

    @PostMapping
    public Lampu create(@RequestBody Lampu lampu){
        return lampuService.createLampu(lampu);
    }
}
