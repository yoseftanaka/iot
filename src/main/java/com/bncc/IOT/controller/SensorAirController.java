package com.bncc.IOT.controller;

import com.bncc.IOT.domain.SensorAir;
import com.bncc.IOT.service.SensorAirService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/sensors")
public class SensorAirController {
    @Autowired
    private SensorAirService sensorAirService;

    @GetMapping
    public List<SensorAir> findAll() {
        return sensorAirService.getAllSensor();
    }

    @PutMapping("/{id}")
    public SensorAir update(@PathVariable int id, @RequestBody SensorAir sensorAir){
        return sensorAirService.updateNilaiSensor(id, sensorAir);
    }

    @PostMapping
    public SensorAir create(@RequestBody SensorAir sensorAir){
        return sensorAirService.createNilaiSensor(sensorAir);
    }
}
