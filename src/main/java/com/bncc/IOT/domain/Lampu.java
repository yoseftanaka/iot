package com.bncc.IOT.domain;

import com.bncc.IOT.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Lampu extends BaseEntity implements Serializable {
    private String nama_lampu;
    private boolean status_lampu;

    public String getNama_lampu() {
        return nama_lampu;
    }

    public void setNama_lampu(String nama_lampu) {
        this.nama_lampu = nama_lampu;
    }

    public boolean isStatus_lampu() {
        return status_lampu;
    }

    public void setStatus_lampu(boolean status_lampu) {
        this.status_lampu = status_lampu;
    }
}
