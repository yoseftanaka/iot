package com.bncc.IOT.domain;

import com.bncc.IOT.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SensorAir extends BaseEntity implements Serializable {
    private int nilai;
    private String kode_sensor_air;

    public int getNilai() {
        return nilai;
    }

    public void setNilai(int nilai) {
        this.nilai = nilai;
    }

    public String getKode_sensor_air() {
        return kode_sensor_air;
    }

    public void setKode_sensor_air(String kode_sensor_air) {
        this.kode_sensor_air = kode_sensor_air;
    }
}
