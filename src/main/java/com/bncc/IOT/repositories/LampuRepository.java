package com.bncc.IOT.repositories;

import com.bncc.IOT.domain.Lampu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LampuRepository extends JpaRepository<Lampu, Integer> {

}
