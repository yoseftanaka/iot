package com.bncc.IOT.repositories;

import com.bncc.IOT.domain.SensorAir;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SensorAirRepository extends JpaRepository<SensorAir, Integer> {
}
