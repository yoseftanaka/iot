package com.bncc.IOT.service;

import com.bncc.IOT.domain.Lampu;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface LampuService {

    public List<Lampu> getAllLampu();
    public Lampu updateLampu(int id, Lampu lampu);
    public Lampu getLampu(int id);
    public Lampu createLampu(Lampu lampu);
}
