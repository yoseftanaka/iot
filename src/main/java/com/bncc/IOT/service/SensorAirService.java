package com.bncc.IOT.service;

import com.bncc.IOT.domain.SensorAir;

import java.util.List;

public interface SensorAirService {
    public List<SensorAir> getAllSensor();
    public SensorAir updateNilaiSensor(int id, SensorAir sensorAir);
    public SensorAir getNilaiSensor(int id);
    public SensorAir createNilaiSensor(SensorAir sensorAir);
}
