package com.bncc.IOT.service.impl;

import com.bncc.IOT.domain.Lampu;
import com.bncc.IOT.exception.UserNotFoundException;
import com.bncc.IOT.repositories.LampuRepository;
import com.bncc.IOT.service.LampuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LampuServiceImpl implements LampuService {

    @Autowired
    private LampuRepository lampuRepository;


    @Override
    public List<Lampu> getAllLampu() {
        return lampuRepository.findAll();
    }

    @Override
    public Lampu updateLampu(int id, Lampu lampu) {
        Lampu lamps = getLampu(id);
        lamps.setNama_lampu(lampu.getNama_lampu());
        lamps.setStatus_lampu(lampu.isStatus_lampu());
        lampuRepository.save(lamps);
        return lamps;
    }

    public Lampu createLampu(Lampu lampu) {
        lampuRepository.save(lampu);
        return lampu;
    }

    public Lampu getLampu(int id){
        Lampu lampu = lampuRepository.findById(id).get();
        return lampu;
    }
}
