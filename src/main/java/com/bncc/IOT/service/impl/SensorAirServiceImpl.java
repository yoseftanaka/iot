package com.bncc.IOT.service.impl;

import com.bncc.IOT.domain.SensorAir;
import com.bncc.IOT.repositories.SensorAirRepository;
import com.bncc.IOT.service.SensorAirService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SensorAirServiceImpl implements SensorAirService {
    @Autowired
    private SensorAirRepository sensorAirRepository;


    @Override
    public List<SensorAir> getAllSensor() {
        return sensorAirRepository.findAll();
    }

    @Override
    public SensorAir updateNilaiSensor(int id, SensorAir sensorAir) {
        SensorAir newSensorAir = getNilaiSensor(id);
        newSensorAir.setKode_sensor_air(sensorAir.getKode_sensor_air());
        newSensorAir.setNilai(sensorAir.getNilai());
        sensorAirRepository.save(newSensorAir);
        return newSensorAir;
    }

    public SensorAir getNilaiSensor(int id){
        return sensorAirRepository.findById(id).get();
    }

    public SensorAir createNilaiSensor(SensorAir sensorAir){
        sensorAirRepository.save(sensorAir);
        return sensorAir;
    }
}
