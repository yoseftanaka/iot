package com.bncc.IOT.service.impl;

import com.bncc.IOT.domain.Lampu;
import com.bncc.IOT.repositories.LampuRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class LampuServiceImplTest {

    @Autowired
    private LampuRepository lampuRepository;

    @Test
    public void getLampu() {
        Lampu lampu = lampuRepository.findById(3).get();
        assertNull(lampu);
    }
}